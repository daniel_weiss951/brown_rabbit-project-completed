const db = require('../config/sql');

exports.page_1 = function(req, res) {
    db.query(`SELECT title, date, text, image FROM page_1`, function(err, results) {
       if(err) {
          throw err;
       } else {
          console.log(results);
          res.render('page_1', {'title': 'Page 1', results })
       }
    })
}

exports.page_2 = function(req, res) {
   db.query(`SELECT title, date, text, image FROM page_2`, function(err, results) {
      if(err) {
         throw err;
      } else {
         console.log(results);
         res.render('page_2', {'title': 'Page 2', results })
      }
   })
}

exports.page_3 = function(req, res) {
   db.query(`SELECT title, date, text, image FROM page_3`, function(err, results) {
      if(err) {
         throw err;
      } else {
         console.log(results);
         res.render('page_3', {'title': 'Page 3', results })
      }
   })
}