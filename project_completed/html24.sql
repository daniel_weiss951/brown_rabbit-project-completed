-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jul 05, 2020 at 11:13 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `html24`
--

-- --------------------------------------------------------

--
-- Table structure for table `page_1`
--

CREATE TABLE `page_1` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `text` text,
  `image` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_1`
--

INSERT INTO `page_1` (`id`, `title`, `date`, `text`, `image`) VALUES
(1, 'Team Suomi 2009', '07. July 2009 by NBC press', 'Here is the Finnish team ready for action at the Nordic Barista Cup 2009. <br><br><strong>Roman Kolpaktsi, team leader</strong> Age: 33 <br>Roman has worked as a barista at Café Ursula in Helsinki for 3 years, and during this time worked with barista training as well. He is a member of SCAE Finnish Chapter Administration, and has worked in the coffee-industry since 2002. <br>In 2008 Roman participated in an internship program in Nicaragua in co-operation with Café Don Paco. He has a degree in Economics and Hospitality Management.', 'http://nordicbaristacup.com/wp-content/uploads/2011/03/TEAMFinland_Roman.jpg'),
(2, NULL, NULL, '<strong>Petra Morbin</strong> Age: 32 <br><br>Petra won 2nd place in the Finnish Barista Championship 2007. She competed in Finnish team 2007 NBC Copenhagen. She has been working in the restaurant business for over 10 years, among other things as a superior bartender and head waiter. Today she works as a teacher in culinary institute.', 'http://nordicbaristacup.com/wp-content/uploads/2011/03/TEAMFinland_Petra.jpg'),
(3, NULL, NULL, '<strong>Mikko Haahti</strong> Age: 25 <br><br>Mikko has worked as a barista at CaféArt in Turku for 5 years with his brother Juhani and rest of the family. He is the Finnish Barista and Latte art champion of 2008 & 2009. Mikko is graphic designer by his education.', 'http://nordicbaristacup.com/wp-content/uploads/2011/03/TEAMFinland_Hahhti.jpg'),
(4, NULL, NULL, '<strong>Heidi Randström</strong> Age: 21 <br><br>Heidi is a barista and has worked at Café Strindberg since 2008. She is new in the field, but very inspired and eager to learn. Heidi is an active team worker.', 'http://nordicbaristacup.com/wp-content/uploads/2011/03/TEAMFinland_Heidi.jpg'),
(5, 'Lost In Iceland With A Thermos', '18. June 2009 by NBC press ', 'The committee for the Nordic Barista Cup 2009 has been meeting and planning the competition for a while now. Here is some information for everyone who will be attending – or is thinking about it: <br><br>The title of the 2009 competition is “Lost in Iceland with a thermos”. <br><br>We will be spending some time in the Icelandic countryside, although the majority of the time will be spent in the centre of Reykjavik. <br><br>To secure the pastoral idyll we ask you to bring your sleeping bag and some outdoors clothes. <br><br>Here are some words to give you some ideas about Nordic Barista Cup 2009: <br><br><strong>Costa Rica</strong><br><strong>Art/Music</strong><br><strong>Outdoor/swimming</strong><strong><br>Thermos</strong><strong><br>Basic grinder</strong><strong><br>Basic roaster</strong><strong><br>Cup of Excellence</strong> <br>Further information will be presented later here. <br>You can sign up for the event now if you <a href=\"#\">click here.</a>', 'http://nordicbaristacup.com/upload/s_h_thermos.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `page_2`
--

CREATE TABLE `page_2` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `text` text,
  `image` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_2`
--

INSERT INTO `page_2` (`id`, `title`, `date`, `text`, `image`) VALUES
(1, 'Nordic Barista Cup Hotel / Hostel', '14. May 2009 by NBC press ', 'We have made deals with one hostel and one hotel for the Nordic Barista Cup. Please mention “Nordic Barista Cup” when you book your room/bed. This way you will get a discount on the price. <br><br>REMEMBER that we are in the countryside Thursday and Friday night, so you do not need to book for these two nights. <br><br><strong>HOTEL ARNARHVOLL</strong> Modern and brand new hotel located down town Reykjavík. The hotel has a restaurant on 8th floor with magnificent views of Reykjavík Bay. Nice and comfortable rooms with mini bar and free internet. Remember to mention Nordic Barista Cup when you order. <br><br><strong>REMEMBER</strong> that we are in the countryside Thursday and Friday night, so you do not need to book for these two nights. For Arnarhvoll website click <a href=\"#\">here.</a> <br><br><strong>BOOKING ARNARHVOLL:</strong> Send email to groups@centerhotels.is and refer to the “Nordic Barista Cup allotment”.Do not book via their webpage, cause the prices are higher there! <br><br><strong>DOWNTOWN HOSTEL</strong> Modern and brand new hostel located down town Reykjavík. For 2 nights (Wednesday night and Saturday night) the hostel will be an exclusive Nordic Barista Cup hostel. We have the whole hostel to our selves (70 beds). There is a lounge area and guest kitchen. The hostel has spacious dorms with 8-10 beds, dorms with 4 beds, family rooms which accommodate up to 5 persons and double rooms with shared or private facilities. You can bring your own bed linen/sleeping bag or you can rent bed linen at the hostel. <br><br><strong>REMEMBER</strong> that we are in the countryside Thursday and Friday night, so you do not need to book for these two nights. For Downtown hostel website click <a href=\"#\">here</a>. <br><br><strong>BOOKING DOWN TOWN HOSTEL:</strong> Send email to <a href=\"#\"> reykjavikdowntown@hostel.is</a> and mention \"Nordic Barista Cup group booking\".', ''),
(2, 'Nordic Barista Cup 17-19 September 2009', '24. April 2009 by NBC press ', 'You can find information about the event by clicking <a href=\"#\">here</a>.', 'http://nordicbaristacup.com/wp-content/uploads/2011/02/IcelandFlags1-138x141.jpg'),
(3, 'Knowledge Of Why Customers Come And Why The Return', '13. November 2007 by NBC press', 'By Per Bengtson, Winner Of White Guide In Service 2007', 'https://dummyimage.com/130x132/03d1ff/030303'),
(4, 'Nordic Roaster 2007', '26. September 2007 by NBC press ', '<br>The second day of the 2007 Nordic Barista Cup is underway! <br><br>This morning features the Nordic Roasters Competition. This is a new competition this year: 10 roasters from the Nordic countries have submitted coffees to be blind judged by the attendees of the NBC.  <br><br>The five teams — Norway, Sweden, Denmark, Finland, and Iceland — are hard at work in their “cafes” preparing the 10 coffee submissions as both filter drip coffee and espresso. <br><br>Attendees are winding their way around the big white tent, trays and ballots in hand, as they make their way toward each team’s cafe to collect their samples. <br><br>Some of us are using cupping spoons to taste the coffees; some of us are simply sipping from the cups. <br><br>So we taste 10 coffees and mark down our favorite on our ballot, drop the ballot in the voting box, and wait for the results! We will find out the winner at lunchtime!', 'https://dummyimage.com/130x132/03d1ff/030303'),
(5, 'Coffee brewing competition', '15. April 2006 by NBC press ', '<br>The NBC-baristas were not only evaluated by the judges at the competitions. The attendants were also given a vote to find the team which brewed the best coffee. <br><br>The first two days of the NBC the national teams roasted their own beans, grinded them and then brewed their own coffee, to be served for the attendants for blind cupping. <br><br>The result was: <br>Denmark<br>Iceland<br>Finland<br>Norway<br>Sweden', '');

-- --------------------------------------------------------

--
-- Table structure for table `page_3`
--

CREATE TABLE `page_3` (
  `id` int(11) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `date` varchar(100) DEFAULT NULL,
  `text` text,
  `image` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_3`
--

INSERT INTO `page_3` (`id`, `title`, `date`, `text`, `image`) VALUES
(1, 'Welcome To Day 2!', '21. August 2006 by NBC press ', 'Thanks to the talented staff (chefs and knowledgeable waiters included), last night’s festivities left a few (er, make that more than a few) a bit bleary-eyed this morning. The combination of superb cuisine and spectacular wines and beers, plus leisurely service and long, pleasant conversations and revelry, was a lot to digest after such a full day of activities. But at the Nordic Barista Cup, some sort of strange energy comes over everyone, giving us all the capability to reenergize almost magically. <br><br>Perhaps it is because we know that the activities to come will be just as good if not better than the ones before. Teams drifted through the doors to the event space around 8 a.m., ready for a lesson from Zander Nosler about the history of the Clover, which has been quite a focus of attention since the NBC began yesterday. Attendees were noticeably absent, most likely catching up on some sleep! <br><br>First things first: Barista teams were directed to a special informational session with the judges, where they were briefed on what would take place over the course of the day: Informative discussions and tastings of sugar from Mr. Lars Bo Jorgensen of Danisco to follow the Clover session. <br><br>Remember how we told you yesterday about the goodie bags all teams and competitors received upon registration at the beginning of the event? NBC Organizer Jens Norgaard relieved the barista teams’ curiosity about the jump rope included in the bag by asking team members to take it out and look at the counter (each rope is equipped with a counter to track the number of “skips” on each rope). Then he walked through the room and recorded the number of skips on each rope. As most of the baristas had yet to even take the ropes out of the packaging, the scores for number of skips were low (except for Icelandic team coach Ragga, who had been getting some exercise and thus earned victory for the Icelanders). <br><br>But the rope-skipping is not over yet! Tim instructed the baristas to “get skipping,” hinting that the counters on their jump ropes would be measured again at some point over the weekend. And so, weary baristas (and yes, Norwegian team, awake dancing until 2:30 in the morning at Noma, we mean you!) began the process of skipping rope in the event space before the first airpots of coffee were even brought out! <br>—Sarah Allen', 'http://nordicbaristacup.com/upload/jumprope1.jpg'),
(2, 'Sweet Stuff', '20. August 2006 by NBC press ', 'Why would terribly serious baristas who prefer their espresso with hardly any accoutrements care to learn about sugar? It’s quite simple actually: they can’t call themselves professionals unless they truly understand not only the science behind each and every ingredient that comes into contact with their beloved brew, but they must also understand the desires of their customers, as well – and we all know how common consumers’ use of sugar and syrup is in their coffee and espresso drinks. <br><br>Who better to explain the science of sugar to the barista teams and the attendees than Mr. Lars Bo Jorgensen of Danisco? Not only did he explain the chemical properties of sucrose, fructose, dextrose and another sucrose to the audience, but he tested them on their abilities to taste them and identify levels of intensity and to distinguish between these different sugars. He also tested the barista teams, and the results of whose palates proved the most in tune to sugars will be revealed with today’s winner at the end of the night. <br>—Sarah Allen', 'http://nordicbaristacup.com/upload/Sugar1.jpg'),
(3, 'Yet Another Delicious Meal', '19. August 2006 by NBC press ', 'Here’s a hint: if you visit Copenhagen, you simply must try the food at Café Europa. Me, I’ve already eaten there four times since I arrived in the city last Monday. <br><br>The lox is like butter, melty goodness in your mouth complemented by an extremely smooth egg puree. And that’s just one item on an extensive menu. I bring this up because, courtesy of Bunn, we were treated to open-faced sandwiches (traditional smorrebrod in Denmark) from Café Europa today. So there is something else everyone here shares besides a passion for coffee: an intensely satisfying lunch experience. <br><br>—Sarah Allen', 'http://nordicbaristacup.com/upload/europabox1.jpg'),
(4, 'They’d Sell You The Shirts Off Their Backs', '13. August 2006 by NBC press ', 'The clouds continued to hold off the rain here in the public square of Nytorv while the barista teams hit the pavement with coffee in one hand a pail in the other. They were looking to make a deal with whomever they could find. Whatever you were willing to pay (and hopefully just a little bit more!) they’d take, and in exchange you’d get a cup of coffee or a latte or even whole bean. <br><br>Meanwhile, the teams took turns at the Probatino, roasting up fresh batches of coffee (which will be used later in another competition) with Probat’s roastmaster Arno Schweck. <br><br>The tally for what each team raised is still to come, but they certainly were putting their hearts into it. Selling El Salvadoran coffee to fight malnutrition in El Salvador has a nice symmetry to it, and obviously the teams were taking their role seriously. But, as this is the Nordic Barista Cup, they made sure to have fun too. <br><br>–Kenneth R Olson', 'http://nordicbaristacup.com/upload/DenmarkRoasting1.jpg'),
(5, 'Lucky Clover!', '11. August 2006 by NBC press ', 'Any lingering questions competitors and attendees might have had about the Clover brewing machine were put to rest with a competition pitting the five Nordic Barista teams (joined by the Estonian team!) against each other brewing the El Salvadoran beans each had roasted that very morning on the Probatino. <br><br>Their coffees had only a few hours to rest, of course, making the fact that the teams were able to evoke flavorful and complex roast profiles. Teams served to a group of judges pulled from the attendee pool. <br> Each team was given 10 minutes on one of two Clovers to adjust the grind, temperature and dose amount before serving samples of their roast to the judges. Ten judges were grouped in pairs to evaluate the roasts. As with the Noma ingredient competition, the winner of the Clover competition will be announced at tonight’s Gala dinner. <br> — Sarah Allen', 'http://nordicbaristacup.com/upload/CloverComp1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `slider_images`
--

CREATE TABLE `slider_images` (
  `id` int(11) NOT NULL,
  `image_1` varchar(100) DEFAULT NULL,
  `image_2` varchar(100) DEFAULT NULL,
  `image_3` varchar(100) DEFAULT NULL,
  `image_4` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `slider_images`
--

INSERT INTO `slider_images` (`id`, `image_1`, `image_2`, `image_3`, `image_4`) VALUES
(1, 'image_1.jpeg', 'image_2.jpeg', 'image_3.jpeg', 'image_4.jpeg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `page_1`
--
ALTER TABLE `page_1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_2`
--
ALTER TABLE `page_2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_3`
--
ALTER TABLE `page_3`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_images`
--
ALTER TABLE `slider_images`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `page_1`
--
ALTER TABLE `page_1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `page_2`
--
ALTER TABLE `page_2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `page_3`
--
ALTER TABLE `page_3`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `slider_images`
--
ALTER TABLE `slider_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
