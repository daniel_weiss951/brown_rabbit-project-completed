const { sweden2010 } = require('../controllers/sweden2010.controller');

module.exports = function (app) {
    app.get('/sweden-2010', sweden2010);
}