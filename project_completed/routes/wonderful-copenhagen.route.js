const { wonderfulCopenhagen } = require('../controllers/wonderfulCopenhagen.controller');

module.exports = function (app) {
    app.get('/wonderful-copenhagen', wonderfulCopenhagen)
}